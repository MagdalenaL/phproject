CREATE DATABASE  IF NOT EXISTS `phprojekt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `phprojekt`;
-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: phprojekt
-- ------------------------------------------------------
-- Server version	5.5.35-0ubuntu0.13.10.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `artist` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_categories`
--

DROP TABLE IF EXISTS `blog_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_categories` (
  `idcategory` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_categories`
--

LOCK TABLES `blog_categories` WRITE;
/*!40000 ALTER TABLE `blog_categories` DISABLE KEYS */;
INSERT INTO `blog_categories` VALUES (2,'Category2'),(3,'Category3'),(4,'Category4');
/*!40000 ALTER TABLE `blog_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_comments`
--

DROP TABLE IF EXISTS `blog_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_comments` (
  `idcomment` int(11) NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET latin1 NOT NULL,
  `published_date` date DEFAULT NULL,
  `idpost` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcomment`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_comments`
--

LOCK TABLES `blog_comments` WRITE;
/*!40000 ALTER TABLE `blog_comments` DISABLE KEYS */;
INSERT INTO `blog_comments` VALUES (1,'ccdfv dfvfv wvwrv','2014-05-30',1,1),(2,'sdcsc fgbfggf','2014-05-30',1,1),(3,'ccdfv dfvfv wvwrv','2014-05-30',1,1),(4,'ccdfv dfvfv wvwrv','2014-05-30',1,1),(5,'ccdfv dfvfv wvwrv','2014-05-30',1,1),(6,'sdcsd gfbfgbhg ik,uk, sdcsd','2014-06-02',2,1),(7,'scsdc vdfc dskjcs csdvdfvks    vdfvdf','2014-06-02',2,1),(8,'dc v jyujy dcdsc rtgt njyn','2014-06-02',2,1),(9,'dc v jyujy dcdsc rtgt njyn','2014-06-02',2,1),(10,'dc v jyujy dcdsc rtgt njyn','2014-06-02',2,1);
/*!40000 ALTER TABLE `blog_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_posts`
--

DROP TABLE IF EXISTS `blog_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_posts` (
  `idpost` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(45) CHARACTER SET latin1 NOT NULL,
  `content` text CHARACTER SET latin1 NOT NULL,
  `published_date` date DEFAULT NULL,
  `idcategory` int(11) NOT NULL,
  PRIMARY KEY (`idpost`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_posts`
--

LOCK TABLES `blog_posts` WRITE;
/*!40000 ALTER TABLE `blog_posts` DISABLE KEYS */;
INSERT INTO `blog_posts` VALUES (8,'Lorem ipsum 1','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent consequat, dolor at congue varius, nunc leo laoreet odio, vitae malesuada justo odio id sem. Donec gravida nibh ac velit semper, sodales egestas mi rutrum. Proin nec velit nec arcu semper auctor eu et ante. Nam vitae pellentesque metus, et iaculis arcu. Vivamus tempor vulputate odio, a adipiscing est fermentum quis. Suspendisse porta congue tincidunt. Mauris mollis ante rhoncus velit varius feugiat. Sed nisl elit, ullamcorper ut massa id, vestibulum fringilla felis. Nunc nibh leo, varius vel laoreet vel, volutpat eget est. Proin dapibus tellus sit amet tortor congue, at scelerisque lorem scelerisque. Proin et lorem congue, feugiat mauris ut, semper est. Mauris nisl massa, consectetur id viverra eu, ullamcorper sit amet dolor.','2014-05-20',4),(9,'Lorem ipsum 2','Phasellus malesuada volutpat hendrerit. Aenean ultrices leo leo, in accumsan lorem ultricies facilisis. Vivamus ac consectetur augue. Etiam ac neque eu mi dictum faucibus. Morbi sodales massa eu orci varius cursus eget in sapien. Vivamus dui lorem, lacinia sit amet accumsan nec, commodo vel urna. In hac habitasse platea dictumst. Phasellus at nisl venenatis, lobortis nibh vel, rhoncus magna. Maecenas eros risus, tincidunt dapibus dui eu, malesuada consequat orci. Etiam imperdiet, velit fringilla volutpat luctus, ante enim facilisis orci, vitae adipiscing nisi risus vel odio. Etiam massa risus, tempus at pharetra sit amet, iaculis quis purus. Nam volutpat varius condimentum. Curabitur ut condimentum est, ac tempor odio. Nulla aliquam aliquam ligula convallis hendrerit. Cras molestie at purus a iaculis. Nam eu quam a felis vestibulum tempor eget eu dolor.','2014-06-02',2),(10,'Lorem ipsum 3','Morbi eu justo dapibus, vestibulum lectus vel, tempor turpis. Donec vel porta libero, quis ullamcorper arcu. Maecenas fermentum urna et enim tristique, nec vulputate purus mollis. Integer semper, sem sit amet tincidunt tempus, nibh dolor porttitor lacus, non bibendum purus metus nec tortor. Phasellus porttitor porta ante, sit amet tincidunt eros molestie eget. Ut pellentesque pellentesque justo eu facilisis. In aliquam mi sit amet nunc molestie, et posuere augue faucibus. Morbi interdum, quam in semper condimentum, augue metus tempor enim, ac malesuada diam tellus in nisi. Nullam sapien turpis, molestie ut vestibulum at, malesuada id ipsum. Cras pretium dui hendrerit, pellentesque purus et, pharetra ligula. Aliquam tellus nibh, tincidunt eu pellentesque vel, faucibus congue erat.','2014-06-02',2),(11,'Lorem ipsum 4','Morbi eu justo dapibus, vestibulum lectus vel, tempor turpis. Donec vel porta libero, quis ullamcorper arcu. Maecenas fermentum urna et enim tristique, nec vulputate purus mollis. Integer semper, sem sit amet tincidunt tempus, nibh dolor porttitor lacus, non bibendum purus metus nec tortor. Phasellus porttitor porta ante, sit amet tincidunt eros molestie eget. Ut pellentesque pellentesque justo eu facilisis. In aliquam mi sit amet nunc molestie, et posuere augue faucibus. Morbi interdum, quam in semper condimentum, augue metus tempor enim, ac malesuada diam tellus in nisi. Nullam sapien turpis, molestie ut vestibulum at, malesuada id ipsum. Cras pretium dui hendrerit, pellentesque purus et, pharetra ligula. Aliquam tellus nibh, tincidunt eu pellentesque vel, faucibus congue erat.','2014-06-02',3),(12,'Lorem ipsum 5','Ut euismod mi vel libero elementum, quis commodo massa fermentum. In vehicula dapibus turpis eget egestas. Sed suscipit erat ac mi malesuada auctor. Vivamus ut placerat lectus. Etiam ultricies fringilla quam, vitae rutrum risus commodo sed. Nulla consequat risus id nulla tincidunt, eu commodo nisl feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam elementum lectus nec elementum mattis. Quisque vehicula tortor sit amet nulla eleifend bibendum. Sed vel eleifend libero, ut dictum elit. Curabitur ullamcorper molestie dui in consectetur. Pellentesque ut fringilla orci. Nam laoreet consequat faucibus','2014-06-02',2),(13,'Lorem ipsum 6','Ut euismod mi vel libero elementum, quis commodo massa fermentum. In vehicula dapibus turpis eget egestas. Sed suscipit erat ac mi malesuada auctor. Vivamus ut placerat lectus. Etiam ultricies fringilla quam, vitae rutrum risus commodo sed. Nulla consequat risus id nulla tincidunt, eu commodo nisl feugiat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam elementum lectus nec elementum mattis. Quisque vehicula tortor sit amet nulla eleifend bibendum. Sed vel eleifend libero, ut dictum elit. Curabitur ullamcorper molestie dui in consectetur. Pellentesque ut fringilla orci. Nam laoreet consequat faucibus','2014-06-02',2),(14,'Lorem ipsum 7','Suspendisse eu urna auctor, fringilla velit eget, commodo diam. Pellentesque nec nunc velit. Pellentesque et congue tellus. Praesent libero lacus, pretium id faucibus quis, accumsan ac neque. Cras at posuere dui, sed interdum sem. Pellentesque nisl mauris, fermentum quis fringilla ac, euismod eu ante. Pellentesque pretium quis eros a porta. Proin enim elit, faucibus nec tincidunt at, consectetur vitae risus. Donec ut dolor et libero lacinia semper. Donec ultrices vitae lectus nec ullamcorper. Integer tincidunt felis posuere tellus pretium, ac lobortis sapien accumsan. Donec luctus risus quis leo tincidunt, id iaculis ipsum fringilla.','2014-06-02',4);
/*!40000 ALTER TABLE `blog_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_posts_tags`
--

DROP TABLE IF EXISTS `blog_posts_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_posts_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpost` int(11) NOT NULL,
  `idtag` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_posts_tags`
--

LOCK TABLES `blog_posts_tags` WRITE;
/*!40000 ALTER TABLE `blog_posts_tags` DISABLE KEYS */;
INSERT INTO `blog_posts_tags` VALUES (4,8,1),(6,9,1),(7,9,2);
/*!40000 ALTER TABLE `blog_posts_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blog_tags`
--

DROP TABLE IF EXISTS `blog_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog_tags` (
  `idtag` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(25) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`idtag`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog_tags`
--

LOCK TABLES `blog_tags` WRITE;
/*!40000 ALTER TABLE `blog_tags` DISABLE KEYS */;
INSERT INTO `blog_tags` VALUES (1,'Tag1'),(2,'Tag2');
/*!40000 ALTER TABLE `blog_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu`
--

DROP TABLE IF EXISTS `menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`idmenu`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu`
--

LOCK TABLES `menu` WRITE;
/*!40000 ALTER TABLE `menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_pages`
--

DROP TABLE IF EXISTS `menu_pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_pages` (
  `idmenu` int(11) NOT NULL,
  `idpage` int(11) NOT NULL,
  `order` char(15) CHARACTER SET latin1 DEFAULT NULL,
  PRIMARY KEY (`idmenu`,`idpage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_pages`
--

LOCK TABLES `menu_pages` WRITE;
/*!40000 ALTER TABLE `menu_pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `menu_pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `idpage` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(45) CHARACTER SET latin1 NOT NULL,
  `link` char(45) CHARACTER SET latin1 NOT NULL,
  `active` tinyint(2) NOT NULL,
  PRIMARY KEY (`idpage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_attributes`
--

DROP TABLE IF EXISTS `pages_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_attributes` (
  `idattribute` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(45) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`idattribute`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_attributes`
--

LOCK TABLES `pages_attributes` WRITE;
/*!40000 ALTER TABLE `pages_attributes` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_values`
--

DROP TABLE IF EXISTS `pages_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages_values` (
  `idpage` int(11) NOT NULL,
  `idattributes` int(11) NOT NULL,
  `value` text CHARACTER SET latin1,
  PRIMARY KEY (`idpage`,`idattributes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_values`
--

LOCK TABLES `pages_values` WRITE;
/*!40000 ALTER TABLE `pages_values` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_values` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_feedback`
--

DROP TABLE IF EXISTS `project_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_feedback` (
  `idfeedback` int(11) NOT NULL AUTO_INCREMENT,
  `content` text CHARACTER SET latin1 NOT NULL,
  `idproject` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `published_date` date NOT NULL,
  PRIMARY KEY (`idfeedback`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_feedback`
--

LOCK TABLES `project_feedback` WRITE;
/*!40000 ALTER TABLE `project_feedback` DISABLE KEYS */;
INSERT INTO `project_feedback` VALUES (1,'sdcdsc dvvf sdcsw egerfv sddcsc',1,1,'2014-06-02');
/*!40000 ALTER TABLE `project_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_photos`
--

DROP TABLE IF EXISTS `project_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_photos` (
  `idphoto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idproject` varchar(45) COLLATE utf8_bin NOT NULL,
  `name` char(48) COLLATE utf8_bin NOT NULL,
  `alt` char(48) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idphoto`),
  UNIQUE KEY `UQ_files_1` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_photos`
--

LOCK TABLES `project_photos` WRITE;
/*!40000 ALTER TABLE `project_photos` DISABLE KEYS */;
INSERT INTO `project_photos` VALUES (1,'1','1ez63kmxu3tk5nt0vhqwgk9ng4ifr9iz.jpg','hdsd'),(2,'1','pv7349v887egz553wktkyse986ak1yio.jpg','ulotka');
/*!40000 ALTER TABLE `project_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_ratings`
--

DROP TABLE IF EXISTS `project_ratings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_ratings` (
  `idrate` int(11) NOT NULL AUTO_INCREMENT,
  `rate` tinyint(5) NOT NULL,
  `published_date` date NOT NULL,
  `idproject` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  PRIMARY KEY (`idrate`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_ratings`
--

LOCK TABLES `project_ratings` WRITE;
/*!40000 ALTER TABLE `project_ratings` DISABLE KEYS */;
INSERT INTO `project_ratings` VALUES (1,4,'2014-06-03',1,1),(2,3,'2014-06-02',1,2),(3,5,'2014-06-03',1,1),(4,3,'2014-06-03',3,1);
/*!40000 ALTER TABLE `project_ratings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `idproject` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(45) CHARACTER SET latin1 NOT NULL,
  `description` text CHARACTER SET latin1,
  PRIMARY KEY (`idproject`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects`
--

LOCK TABLES `projects` WRITE;
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` VALUES (1,'Lorem project 1','Vivamus aliquet mattis leo ullamcorper vulputate. Nullam velit nunc, malesuada quis mi id, consequat vulputate urna. Mauris quis risus nibh. Mauris molestie fringilla accumsan. Morbi vehicula, nunc non vehicula molestie, nunc neque porta erat, in euismod est lectus pulvinar risus. Sed non risus vehicula mi suscipit pellentesque. Sed et nibh quis tortor ullamcorper vulputate at sed libero. Cras et turpis porta, consectetur ante varius, pretium arcu. Nulla posuere venenatis odio, a bibendum sapien varius a. Nulla facilisi.'),(3,'Lorem project 2','Maecenas vehicula scelerisque massa, nec rutrum turpis laoreet vel. Aenean mattis dolor ac augue eleifend faucibus. Donec suscipit, arcu at interdum cursus, lorem ligula dictum urna, non tristique tortor enim sollicitudin nunc. Sed lobortis tortor in nunc elementum adipiscing. Aenean mollis pharetra justo, eget suscipit ligula gravida et. Nam sit amet erat sagittis, sagittis quam non, tristique tortor. Mauris sodales vel urna pulvinar semper. Mauris metus justo, molestie nec felis at, posuere tincidunt nisl.');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_photos`
--

DROP TABLE IF EXISTS `projects_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_photos` (
  `idphoto` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(48) COLLATE utf8_bin NOT NULL,
  `alt` char(48) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`idphoto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_photos`
--

LOCK TABLES `projects_photos` WRITE;
/*!40000 ALTER TABLE `projects_photos` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `idrole` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idrole`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'ROLE_ADMIN'),(2,'ROLE_USER'),(3,'ROLE_ANONYMUS'),(4,'ROLE_UNCONFIRMED');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` char(45) CHARACTER SET latin1 NOT NULL,
  `email` char(125) CHARACTER SET latin1 NOT NULL,
  `homesite` char(125) CHARACTER SET latin1 DEFAULT NULL,
  `signupdate` date NOT NULL,
  `password` char(255) CHARACTER SET latin1 NOT NULL,
  `hash` char(255) CHARACTER SET latin1 NOT NULL,
  `userstatus` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','magdalena.limanowka@gmail.com',NULL,'2014-04-12','Lev4gotN3ZSFGNsv6Us2InQ09NjH8sVba2WR76YAsRrn2iL9mLfdXCdOWNOtcX7cFzSQRZ9deafIwyjdd76P9A==','',1),(4,'Magda','magdalena.limanowka@no-restrictions.com','magdalena.limanowka.com.pl','2014-06-02','9U7eETtSfwHvc','e720a4a753dec2612519471c41dae430',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `iduser` int(11) DEFAULT NULL,
  `idrole` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_roles`
--

LOCK TABLES `users_roles` WRITE;
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` VALUES (1,1,1);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-06-04 11:49:12

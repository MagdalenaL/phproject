<?php

namespace Model;

use Doctrine\DBAL\DBALException;
use Silex\Application;

class PhotosModel
{

    protected $_db;

    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    public function savePhoto($name, $data)
    {
        $sql = 'INSERT INTO `project_photos` (`name`, `alt`, `idproject`) VALUES (?,?,?)';
        $this->_db->executeQuery($sql, array($name, $data['alt'], $data['idproject']));
    }

    public function createName($name)
    {
        $newName = '';
        $ext = pathinfo($name, PATHINFO_EXTENSION);
        $newName = $this->_randomString(32) . '.' . $ext;

        while(!$this->_isUniqueName($newName)) {
            $newName = $this->_randomString(32) . '.' . $ext;
        }

        return $newName;
    }

    public function getPhotosByProject($idproject)
    {
        $sql = 'SELECT * FROM project_photos WHERE idproject = ?';
        return$this->_db->fetchAll($sql, array($idproject));
    }

    protected function _randomString($length)
    {
        $string = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));
        for ($i = 0; $i < $length; $i++) {
            $string .= $keys[array_rand($keys)];
        }
        return $string;
    }

    protected function _isUniqueName($name)
    {
        $sql = 'SELECT COUNT(*) AS files_count FROM project_photos WHERE name = ?';
        $result = $this->_db->fetchAssoc($sql, array($name));
        return !$result['files_count'];
    }

}
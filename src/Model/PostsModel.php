<?php

namespace Model;

use Silex\Application;

/**
 * Class PostsModel
 *
 * @class PostsModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class PostsModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Gets one post.
     *
     * @access public
     * @param Integer $idpost
     * @return Array Associative array contains all information about this one post.
     */
    public function getPost($idpost)
    {
        $sql = 'SELECT * FROM blog_posts WHERE idpost = ? LIMIT 1';

        return $this->_db->fetchAssoc($sql, array($idpost));
    }

    /**
     * Gets all posts
     *
     * @access public
     * @return Array Posts array
     */
    public function getPostList()
    {
        $sql = 'SELECT * FROM blog_posts';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Puts one post.
     *
     * @access public
     * @param  Array $data Associative array contains posts title, content, pubished date and id category.
     * @return Void
     */
    public function addPost($data)
    {
        $sql = 'INSERT INTO blog_posts (title, content, published_date, idcategory) VALUES (?,?,?,?)';
        $this->_db->executeQuery($sql, array($data['title'], $data['content'],  $data['published_date'], $data['category']));
    }

    /**
     * Updates one post.
     *
     * @access public
     * @param Array $data Associative array contains posts title, content, pubished date and id category.
     * @return Void
     */
    public function editPost($data)
    {

        if (isset($data['id']) && ctype_digit((string)$data['id'])) {
            $sql = 'UPDATE blog_posts SET title = ?, content = ?, published_date = ? , idcategory = ? WHERE idpost = ?';
            $this->_db->executeQuery($sql, array($data['title'], $data['content'],  $data['published_date'], $data['category'], $data['id']));
        }else{
            $sql = 'INSERT INTO blog_posts (title, content, published_date, idcategory) VALUES (?,?,?,?)';
            $this->_db->executeQuery($sql, array($data['title'], $data['content'],  $data['published_date'], $data['category']));
        }
    }

    /**
     * Delete one post.
     *
     * @access public
     * @param Array $data Associative array contains id post.
     * @return Void
     */
    public function deletePost($data)
    {
        $sql = 'DELETE FROM `blog_posts` WHERE `idpost`= ?';
        $this->_db->executeQuery($sql, array($data['idpost']));
    }

    /**
     * Gets one post with his category name
     *
     * @access public
     * @param Integer $idpost
     * @return Array Associative post array
     */
    public function getPostWithCategoryName($idpost)
    {
        $sql = 'SELECT * FROM blog_posts natural join blog_categories where idpost = ? LIMIT 1';

        return $this->_db->fetchAssoc($sql, array($idpost));
    }

    /**
     *
     *
     * @access public
     * @param Integer $limit
     * @return Integer
     */
    public function countPostsPages($limit)
    {
        $pagesCount = 0;
        $sql = 'SELECT COUNT(*) as pages_count FROM blog_posts';
        $result = $this->_db->fetchAssoc($sql);
        if ($result) {
            $pagesCount =  ceil($result['pages_count']/$limit);
        }
        return $pagesCount;
    }

    /**
     *
     *
     * @access public
     * @param Integer $page
     * @param Integer $limit
     * @param Integer $pagesCount
     * @return Array
     */
    public function getPostsPage($page, $limit, $pagesCount)
    {
        if (($page <= 1) || ($page > $pagesCount)) {
            $page = 1;
        }
        $sql = 'SELECT * FROM blog_posts natural join blog_categories LIMIT :start, :limit';
        $statement = $this->_db->prepare($sql);
        $statement->bindValue('start', ($page-1)*$limit, \PDO::PARAM_INT);
        $statement->bindValue('limit', $limit, \PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }


}
<?php

namespace Model;

use Doctrine\DBAL\DBALException;
use Silex\Application;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

/**
 * Class UsersModel
 *
 * @class UsersModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class UsersModel
{

    /**
     * Silex application object
     *
     * @access protected
     * @var $_app Silex\Application
     */
    protected $_app;
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_app = $app;
        $this->_db = $app['db'];
    }

    /**
     * Puts one user to database.
     *
     * @access public
     * @param  Array $data Associative array contains all necessary information.
     * @return Void
     */
    public function Register($data)
    {
        $sql = "INSERT INTO `users` (`nickname`, `email`, `homesite`, `signupdate`, `password`, `hash`) VALUES (?,?,?,?,?,?);";
        return $this->_db->executeQuery($sql, array($data['nickname'], $data['email'], $data['homesite'], $data['signupdate'], $data['password'], $data['hash']));
    }

    /**
     * Updates information about user.
     *
     * @access public
     * @param Array $data Associative array contains all necessary information.
     * @return Void
     */
    public function editUser($data)
    {
        if (isset($data['iduser']) && ctype_digit((string)$data['iduser'])) {
            $sql = 'UPDATE users SET nickname = ?, email = ? WHERE homesite = ?';
            $this->_db->executeQuery($sql, array($data['nickname'],  $data['email'], $data['homesite']));
        }else{
            $sql = "INSERT INTO `users` (`nickname`, `email`, `homesite`, `signupdate`, `password`, `hash`) VALUES (?,?,?,?, ENCRYPT(?),?);";
            $this->_db->executeQuery($sql, array($data['nickname'], $data['email'], $data['homesite'], $data['signupdate'], $data['password'], $data['hash']));
        }
    }

    /**
     * Delete one user. Update users flag.
     *
     * @access public
     * @param Integer $iduser.
     * @return Void
     */
    public function deleteUser($iduser)
    {
        $sql = "UPDATE `users` SET `userstatus`='0' WHERE `iduser`= ?";

        $this->_db->executeQuery($sql, array($iduser));
    }

    public function changePassword($data, $id)
    {
        $sql = "UPDATE `users` SET `password`=? WHERE `iduser`= ?";

        $this->_db->executeQuery($sql, array($data['new_password'],$id));
    }

    /**
     * Load user by login.
     *
     * @access public
     * @param String $login
     * @return Array Information about searching user.
     */
    public function loadUserByLogin($login)
    {
        $data = $this->getUserByLogin($login);

        if (!$data) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $login));
        }

        $roles = $this->getUserRoles($data['iduser']);

        if (!$roles) {
            throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $login));
        }

        $user = array(
            'login' => $data['nickname'],
            'password' => $data['password'],
            'roles' => $roles
        );

        return $user;
    }

    /**
     * Gets user by id.
     *
     * @access public
     * @param Integer $id
     * @return Array Information about searching user.
     */
    public function getUserById($id)
    {
        $sql = 'SELECT * FROM users WHERE `iduser` = ? Limit 1';
        return $this->_db->fetchAssoc($sql, array((int) $id));
    }

    /**
     * Get user by login.
     *
     * @access public
     * @param String $login
     * @return Array Information about searching user.
     */
    public function getUserByLogin($login)
    {
        $sql = 'SELECT * FROM users WHERE nickname = ?';
        return $this->_db->fetchAssoc($sql, array((string) $login));
    }

    /**
     * Get users role.
     *
     * @access public
     * @param String $userId
     * @return Array
     */
    public function getUserRoles($userId)
    {
        $sql = '
            SELECT
                roles.role
            FROM
                users_roles
            INNER JOIN
                roles
            ON users_roles.idrole=roles.idrole
            WHERE
                users_roles.iduser = ?
            ';

        $result = $this->_db->fetchAll($sql, array((string) $userId));

        $roles = array();
        foreach($result as $row) {
            $roles[] = $row['role'];
        }

        return $roles;
    }

    /**
     * Connected user with his role.
     *
     * @access public
     * @param  Integer $iduser
     * @return Void
     */
    public function addRole($iduser)
    {
        $sql = "INSERT INTO `users_roles` (`iduser`, `idrole`) VALUES (?,?,?);";

        $this->_db->executeQuery($sql, array($iduser, '4'));
    }

    /**
     * Confirm user. Change his role
     *
     * @access public
     * @param  Integer $id
     * @return Void
     */
    public function confirmUser($id)
    {
        $sql = "UPDATE `users_roles` SET `idrole`='2' WHERE `iduser`= ?;";

        $this->_db->executeQuery($sql, array($id));
    }

}
<?php

namespace Model;

use Silex\Application;

/**
 * Class CommentModel
 *
 * @class CommentModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class CommentsModel
{
    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Gets one comment.
     *
     * @access public
     * @param Integer $idcomment
     * @return array Associative array contains all information about this one comment.
     */
    public function getComment($idcomment)
    {
        $sql = 'SELECT * FROM blog_comments WHERE idcomment = ? LIMIT 1';
        return $this->_db->fetchAssoc($sql, array($idcomment));
    }

    /**
     * Gets all comments for one post
     *
     * @access public
     * @param Integer $idpost which are for comments
     * @return Array Comment array
     */
    public function getCommentsList($id)
    {
        $sql = 'SELECT * FROM blog_comments WHERE idpost = ?';
        return $this->_db->fetchAll($sql, array($id));
    }

    /**
     * Puts one comment.
     *
     * @access public
     * @param  Array $data Associative array contains comments content, pubished date, idpost and id logged user.
     * @return Void
     */
    public function addComment($data)
    {
        $sql = 'INSERT INTO blog_comments (content, published_date, idpost, iduser) VALUES (?,?,?,?)';
        $this->_db->executeQuery($sql, array($data['content'],  $data['published_date'], $data['idpost'], $data['iduser']));
    }

    /**
     * Updates one comment.
     *
     * @access public
     * @param Array $data Associative array contains id comment, comments content, pubished date, idpost and id logged user.
     * @return Void
     */
    public function editComment($data)
    {

        if (isset($data['idcomment']) && ctype_digit((string)$data['idcomment'])) {
            $sql = 'UPDATE blog_comments SET content = ?, published_date = ? WHERE idcomment = ?';
            $this->_db->executeQuery($sql, array($data['content'],  $data['published_date'], $data['idcomment']));
        }else{
            $sql = 'INSERT INTO blog_comments (content, published_date, idpost, iduser) VALUES (?,?,?,?)';
            $this->_db->executeQuery($sql, array($data['content'],  $data['published_date'], $data['idpost'], $data['idCurrentUser']));
        }
    }

    /**
     * Delete one comment.
     *
     * @access public
     * @param Array $data Associative array contains id comment.
     * @return Void
     */
    public function deleteComment($data)
    {
        $sql = 'DELETE FROM `blog_comments` WHERE `idcomment`= ?';
        $this->_db->executeQuery($sql, array($data['idcomment']));
    }

}
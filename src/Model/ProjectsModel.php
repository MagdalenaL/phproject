<?php

namespace Model;

use Silex\Application;

/**
 * Class ProjectsModel
 *
 * @class ProjectsModel
 * @package Model
 * @author Magdalena Limanówka
 * @link wierzba.wzks.uj.edy.pl/~12_limanowka/PHProjekt
 * @uses Doctrine\DBAL\DBALException
 * @uses Silex\Application
 */
class ProjectsModel
{

    /**
     * Database access object.
     *
     * @access protected
     * @var $_db Doctrine\DBAL
     */
    protected $_db;

    /**
     * Class constructor.
     *
     * @access public
     * @param Appliction $app Silex application object
     */
    public function __construct(Application $app)
    {
        $this->_db = $app['db'];
    }

    /**
     * Gets one project.
     *
     * @access public
     * @param Integer $idproject
     * @return Array Associative array contains all information about this one project.
     */
    public function getProject($idproject)
    {
        $sql = 'SELECT * FROM projects WHERE idproject = ? LIMIT 1';

        return $this->_db->fetchAssoc($sql, array($idproject));
    }

    /**
     * Gets all project.
     *
     * @access public
     * @return Array Associative projects array.
     */
    public function getProjectList()
    {
        $sql = 'SELECT * FROM projects';
        return $this->_db->fetchAll($sql);
    }

    /**
     * Puts one post.
     *
     * @access public
     * @param  Array $data Associative array contains project title and description.
     * @return Void
     */
    public function addProject($data)
    {
        $sql = 'INSERT INTO projects (title, description) VALUES (?,?)';
        $this->_db->executeQuery($sql, array($data['title'], $data['description']));
    }

    /**
     * Updates one post.
     *
     * @access public
     * @param Array $data Associative array contains comments title, content, pubished date and id category.
     * @return Void
     */
    public function editProject($data)
    {

        if (isset($data['id']) && ctype_digit((string)$data['id'])) {
            $sql = 'UPDATE projects SET title = ?, description = ? WHERE idproject = ?';
            $this->_db->executeQuery($sql, array($data['title'], $data['description'], $data['id']));
        }else{
            $sql = 'INSERT INTO projects (title, description) VALUES (?,?)';
            $this->_db->executeQuery($sql, array($data['title'], $data['description']));
        }
    }

    /**
     * Delete one project.
     *
     * @access public
     * @param Array $data Associative array contains id project.
     * @return Void
     */
    public function deleteProject($data)
    {
        $sql = 'DELETE FROM `projects` WHERE `idproject`= ?';
        $this->_db->executeQuery($sql, array($data['idproject']));
    }

    /**
     *
     *
     * @access public
     * @param Integer $limit
     * @return Integer
     */
    public function countProjectsPages($limit)
    {
        $pagesCount = 0;
        $sql = 'SELECT COUNT(*) as pages_count FROM projects';
        $result = $this->_db->fetchAssoc($sql);
        if ($result) {
            $pagesCount =  ceil($result['pages_count']/$limit);
        }
        return $pagesCount;
    }

    /**
     *
     *
     * @access public
     * @param Integer $page
     * @param Integer $limit
     * @param Integer $pagesCount
     * @return Array
     */
    public function getProjectsPage($page, $limit, $pagesCount)
    {
        if (($page <= 1) || ($page > $pagesCount)) {
            $page = 1;
        }
        $sql = 'SELECT * FROM projects LIMIT :start, :limit';
        $statement = $this->_db->prepare($sql);
        $statement->bindValue('start', ($page-1)*$limit, \PDO::PARAM_INT);
        $statement->bindValue('limit', $limit, \PDO::PARAM_INT);
        $statement->execute();
        return $statement->fetchAll();
    }

}
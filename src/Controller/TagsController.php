<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\TagsModel;

class TagsController implements ControllerProviderInterface
{
    protected $_model;

    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new TagsModel($app);
        $tagController = $app['controllers_factory'];
        $tagController->get('/{page}/{idpost}/', array($this, 'index'))->value('page', 1)->bind('/tags/');
        $tagController->match('/add/', array($this, 'add'))->bind('/tags/add');
        $tagController->match('/edit/{idtag}', array($this, 'edit'))->bind('/tags/edit');
        $tagController->match('/delete/{idtag}', array($this, 'delete'))->bind('/tags/delete');
        $tagController->match('/connect/{idpost}', array($this, 'connectTag'))->bind('/tags/connect');
        $tagController->match('/disconnect/{idpost}/{idtag}', array($this, 'disconnectTag'))->bind('/tags/disconnect');
        $tagController->match('/manage/{idpost}', array($this, 'manageTag'))->bind('/tags/manage');
        $tagController->match('/controlpanel/', array($this, 'controlTag'))->bind('/tags/controlpanel');
        return $tagController;
    }

    /*
     *
     */
    public function index(Application $app, Request $request)
    {
        $id = (int)$request->get('idpost', 0);

        $tags = $this->_model->getTagsListByPost($id);

        return $app['twig']->render('tags/index.twig', array('tags' => $tags, 'idpost' => $id));
    }

    /*
     *
     */
    public function add(Application $app, Request $request)
    {


        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('title', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $model = $this->_model->addTag($data);
            if (!$model) {
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New Tag added.'));
                return $app->redirect($app['url_generator']->generate("/posts/"), 301);
            }
        }

        return $app['twig']->render('tags/add.twig', array('form' => $form->createView()));
    }

    /*
     *
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int)$request->get('idtag', 0);

        $tag = $this->_model->getTag($id);

        $data = array(
            'title' => $tag['title'],
            'idtag' => $id
        );

        if (count($tag)) {
            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('title', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $model = $this->_model->editTag($data);
                if (!$model) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Tag update.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('tags/edit.twig', array('form' => $form->createView()));
        } else {
            return $app->redirect($app['url_generator']->generate('/tags/add'), 301);
        }
    }

    /*
     *
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int)$request->get('idtag', 0);

        $tag = $this->_model->getTag($id);

        $data = array();

        if (count($tag)) {
            $redirect = $app->redirect($app['url_generator']->generate('/posts/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idtag', 'hidden', array(
                    'data' => $id,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model = $this->_model->deleteTag($data);
                if (!$model) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Tag delete.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('tags/delete.twig', array('form' => $form->createView(), 'redirect' => $redirect));

        } else {
            $app->notFound();
        }
    }

    public function connectTag(Application $app, Request $request)
    {
        $idpost = (int)$request->get('idpost', 0);

        $tags = $this->_model->getTagsDict();

        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('idtag', 'choice', array(
                'choices' => $tags,
            ))
            ->add('idpost', 'hidden', array(
                'data' => $idpost,
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $model = $this->_model->connectWithPost($data);
            if (!$model) {
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New Tag added.'));
                return $app->redirect($app['url_generator']->generate("/posts/"), 301);
            }
        }

        return $app['twig']->render('tags/connect.twig', array('form' => $form->createView()));
    }

    public function disconnectTag(Application $app, Request $request)
    {
        $idpost = (int)$request->get('idpost', 0);
        $idtag = (int)$request->get('idtag', 0);

        $tag = $this->_model->getTag($idtag);

        $data = array();

        if (count($tag)) {
            $redirect = $app->redirect($app['url_generator']->generate('/posts/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idpost', 'hidden', array(
                    'data' => $idpost,
                ))
                ->add('idtag', 'hidden', array(
                    'data' => $idtag,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model = $this->_model->disconnectWithPost($data);
                if (!$model) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Tag delete.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('tags/delete.twig', array('form' => $form->createView(), 'redirect' => $redirect));

        } else {
            $app->notFound();
        }
    }

    public function manageTag(Application $app, Request $request)
    {
        $id = (int)$request->get('idpost', 0);

        $tags = $this->_model->getTagsListByPost($id);

        return $app['twig']->render('tags/manage.twig', array('tags' => $tags, 'idpost' => $id));
    }

    public function controlTag(Application $app, Request $request)
    {
        $tags = $this->_model->getTagList();
        return $app['twig']->render('tags/control.twig', array('tags' => $tags));
    }
}
<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\CommentsModel;
use Model\UsersModel;

class CommentsController implements ControllerProviderInterface
{
    protected $_model;
    protected $_user;

    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new CommentsModel($app);
        $this->_user = new UsersModel($app);
        $commentController = $app['controllers_factory'];
        $commentController->get('/{page}/{idpost}/', array($this, 'index'))->value('page', 1)->bind('/comments/');
        $commentController->match('/add/{idpost}', array($this, 'add'))->bind('/comments/add');
        $commentController->match('/edit/{id}', array($this, 'edit'))->bind('/comments/edit');;
        $commentController->match('/delete/{id}', array($this, 'delete'))->bind('/comments/delete');;
        return $commentController;
    }

    /*
     *
     */
    public function index(Application $app, Request $request)
    {
        $id = (int) $request->get('idpost', 0);

        $comments = $this->_model->getCommentsList($id);

        return $app['twig']->render('comments/index.twig', array('comments' => $comments, 'idpost'=> $id));
    }

    /*
     *
     */
    public function add(Application $app, Request $request)
    {

        $idpost = (int) $request->get('idpost', 0);
        $iduser = $this->getIdCurrentUser($app);


        // default values:
        $data = array(
            'published_date'=> date('Y-m-d'),
            'idpost'=>$idpost,
            'iduser'=>$iduser,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('content', 'textarea', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $model= $this->_model->addComment($data);
            if(!$model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New comment added.'));
                return $app->redirect($app['url_generator']->generate("/posts/"), 301);
            }
        }

        return $app['twig']->render('comments/add.twig', array('form' => $form->createView(), 'idpost'=>$idpost));
    }

    /*
     *
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int) $request->get('id', 0);
        $idCurrentUser = $this->getIdCurrentUser($app);
        $comment = $this->_model->getComment($id);

            // default values:
            $data = array(
                'idcomment'=>$id,
                'published_date'=>date('Y-m-d'),
                'idpost'=>$comment['idpost'],
                'iduser'=>$comment['iduser'],
                'idCurrentUser'=>$idCurrentUser,
            );

            if(count($comment)){
                $form = $app['form.factory']->createBuilder('form', $data)
                    ->add('content', 'textarea', array(
                        'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                    ))
                    ->getForm();

                $form->handleRequest($request);

                if ($form->isValid()) {
                    $data = $form->getData();

                    $model= $this->_model->editComment($data);
                    if(!$model){
                        $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'comment update.'));
                        return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                    }
                }

                return $app['twig']->render('comments/edit.twig', array('form' => $form->createView()));
            }else{
                return $app->redirect($app['url_generator']->generate('/comments/add'), 301);
            }
    }

    /*
     *
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('id', 0);

        $comment = $this->_model->getComment($id);

        $data=array();

        if (count($comment)) {
            $redirect = $app->redirect($app['url_generator']->generate('/comments/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idcomment', 'hidden', array(
                    'data'=>$id,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model= $this->_model->deleteComment($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'comment delete.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('comments/delete.twig', array('form' => $form->createView(), 'redirect'=>$redirect));

        }else{
            $app->notFound();
        }
    }


    public function getIdCurrentUser($app)
    {

        $login = $this->getCurrentUser($app);
        $iduser = $this->_user->getUserByLogin($login);

        return $iduser['iduser'];


    }

    protected function getCurrentUser($app)
    {
        $token = $app['security']->getToken();

        if (null !== $token) {
            $user = $token->getUser()->getUsername();
        }

        return $user;
    }


}
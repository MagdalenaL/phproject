<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\CategoriesModel;

class CategoriesController implements ControllerProviderInterface
{
    protected $_model;

    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new CategoriesModel($app);
        $categoryController = $app['controllers_factory'];
        $categoryController->get('/{page}/{idcategory}/', array($this, 'index'))->value('page', 1)->bind('/categories/');
        $categoryController->match('/add/', array($this, 'add'))->bind('/categories/add');
        $categoryController->match('/edit/{idcategory}', array($this, 'edit'))->bind('/categories/edit');
        $categoryController->match('/delete/{idcategory}', array($this, 'delete'))->bind('/categories/delete');
        $categoryController->match('/controlpanel/', array($this, 'controlCategory'))->bind('/categories/controlpanel');
        return $categoryController;
    }

    /*
     *
     */
    public function index(Application $app, Request $request)
    {
        $id = (int) $request->get('idcategory', 0);
        $post = $this->_model->getPostsListByIdcategory($id);
        return $app['twig']->render('categories/index.twig', array('posts' => $post));
    }

    /*
     *
     */
    public function add(Application $app, Request $request)
    {

        // default values:
        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('name', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 2)))
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $model= $this->_model->addCategory($data);
            if(!$model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New Category added.'));
                return $app->redirect($app['url_generator']->generate("/posts/"), 301);
            }
        }

        return $app['twig']->render('categories/add.twig', array('form' => $form->createView()));
    }

    /*
     *
     */
    public function edit(Application $app, Request $request)
    {

        $id = (int) $request->get('idcategory', 0);

        $category = $this->_model->getCategory($id);

        // default values:
        $data = array(
            'name'=> $category['title'],
            'idcategory'=>$id
        );

        if(count($category)){
            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('name', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $model= $this->_model->editCategory($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Category update.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('categories/edit.twig', array('form' => $form->createView()));
        }else{
            return $app->redirect($app['url_generator']->generate('/categorys/add'), 301);
        }
    }

    /*
     *
     */
    public function delete(Application $app, Request $request)
    {
        $id = (int) $request->get('idcategory', 0);

        $category = $this->_model->getCategory($id);

        $data=array();

        if (count($category)) {
            $redirect = $app->redirect($app['url_generator']->generate('/posts/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idcategory', 'hidden', array(
                    'data'=>$id,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model= $this->_model->deleteCategory($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Category delete.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('categories/delete.twig', array('form' => $form->createView(), 'redirect'=>$redirect));

        }else{
            $app->notFound();
        }
    }

    public function connectCategory(Application $app, Request $request)
    {
        $idpost = (int) $request->get('idpost', 0);

        $categories = $this->_model->getCategorysDict();

        $data = array();

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('idcategory', 'choice', array(
                'choices' => $categories,
            ))
            ->add('idpost', 'hidden', array(
                'data' => $idpost,
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();
            $model= $this->_model->connectWithPost($data);
            if(!$model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'New Category added.'));
                return $app->redirect($app['url_generator']->generate("/posts/"), 301);
            }
        }

        return $app['twig']->render('categories/connect.twig', array('form' => $form->createView()));
    }

    public function disconnectCategory(Application $app, Request $request)
    {
        $idpost = (int) $request->get('idpost', 0);
        $idcategory = (int) $request->get('idcategory', 0);

        $category = $this->_model->getCategory($idcategory);

        $data=array();

        if (count($category)) {
            $redirect = $app->redirect($app['url_generator']->generate('/posts/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('idpost', 'hidden', array(
                    'data'=>$idpost,
                ))
                ->add('idcategory', 'hidden', array(
                    'data'=>$idcategory,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model= $this->_model->disconnectWithPost($data);
                if(!$model){
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Category delete.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('categories/delete.twig', array('form' => $form->createView(), 'redirect'=>$redirect));

        }else{
            $app->notFound();
        }
    }

    public function controlCategory(Application $app, Request $request)
    {
        $categories = $this->_model->getCategories();
        return $app['twig']->render('categories/control.twig', array('categories'=>$categories));
    }
}
<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\PagesModel;

class PagesController implements ControllerProviderInterface
{
    protected $_model;

    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new PagesModel($app);
        $pagesController = $app['controllers_factory'];
        $pagesController->match('/aboutme/', array($this, 'aboutme'))->bind('/pages/aboutme');
        $pagesController->match('/contact/', array($this, 'contact'))->bind('/pages/contact');
        $pagesController->match('/editaboutme/', array($this, 'editaboutme'))->bind('/pages/editaboutme');;
        $pagesController->match('/editcontact/', array($this, 'editcontact'))->bind('/pages/editcontact');
        return $pagesController;
    }

    public function aboutme(Application $app, Request $request)
    {
        $page = $this->_model->getPage('aboutme');

        $idpage = $page['idpage'];

        $page = $this->_model->getInformation($idpage);

        return $app['twig']->render('pages/aboutme.twig', array('pages'=>$page));
    }

    public function editAboutme(Application $app, Request $request)
    {
        $page = $this->_model->getPage('aboutme');

        $idpage = $page['idpage'];

        $data = array(
            'idpage'=>$idpage,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('name', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('lastname', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('description', 'textarea', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $model= $this->_model->updatePage($data);
            if($model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Information added.'));
                return $app->redirect($app['url_generator']->generate("/pages/aboutme"), 301);
            }
        }

        return $app['twig']->render('pages/edit_aboutme.twig', array('form' => $form->createView()));
    }

    public function contact(Application $app, Request $request)
    {
        $page = $this->_model->getPage('contact');

        $idpage = $page['idpage'];

        $page = $this->_model->getInformation($idpage);

        return $app['twig']->render('pages/contact.twig', array('pages'=>$page));
    }

    public function editContact(Application $app, Request $request)
    {
        $page = $this->_model->getPage('contact');

        $idpage = $page['idpage'];

        $data = array(
            'idpage'=>$idpage,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('email', 'email', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('skype', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('telephone', 'number', array(
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            $model= $this->_model->updatePage($data);
            if($model){
                $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Information added.'));
                return $app->redirect($app['url_generator']->generate("/pages/contact"), 301);
            }
        }

        return $app['twig']->render('pages/edit_contact.twig', array('form' => $form->createView()));
    }

}
<?php

namespace Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;
use Model\UsersModel;

class UsersController implements ControllerProviderInterface
{
    protected $_model;


    /*
     *
     */
    public function connect(Application $app)
    {
        $this->_model = new UsersModel($app);
        $userController = $app['controllers_factory'];
        $userController->match('/add/', array($this, 'add'))->bind('/users/add');
        $userController->match('/edit/{id}', array($this, 'edit'))->bind('/users/edit');;
        $userController->match('/delete/{id}', array($this, 'delete'))->bind('/users/delete');;
        $userController->get('/view/{id}', array($this, 'view'))->bind('/users/view');
        $userController->match('/password/{id}', array($this, 'password'))->bind('/users/password');
        return $userController;
    }


    /*
     *
     */
    public function add(Application $app, Request $request)
    {
        $hash = $this->genHash();

        $data = array(
            'signupdate' => date('Y-m-d'),
            'hash' => $hash,
        );

        $form = $app['form.factory']->createBuilder('form', $data)
            ->add('nickname', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('password', 'password', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('confirm_password', 'password', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 6)))
            ))
            ->add('email', 'email', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->add('homesite', 'text', array(
                'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
            ))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            if ($data['password'] === $data['confirm_password']) {

                $data['password'] = $app['security.encoder.digest']->encodePassword("{$data['password']}", '');

                $model = $this->_model->Register($data);

                if ($model) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Register was succeed'));
                    return $app->redirect($app['url_generator']->generate("/auth/login"), 301);
                }
            } else {
                $app['session']->getFlashBag()->add('message', array('type' => 'warning', 'content' => 'Passwords are not the same'));
                return $app['twig']->render('users/add.twig', array('form' => $form->createView()));
            }
        }

        return $app['twig']->render('users/add.twig', array('form' => $form->createView()));
    }

    /*
     *
     */

    protected function genHash()
    {
        $hash = md5(microtime(true) . mt_rand(0, mt_getrandmax()));

        return $hash;
    }

    /*
     *
     */

    public function edit(Application $app, Request $request)
    {

        $id = (int)$request->get('id', 0);

        $user = $this->_model->getUserById($id);

        $data = array();

        if (count($user)) {

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('nickname', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('email', 'email', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('homesite', 'text', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('password', 'password', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->getForm();


            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $model = $this->_model->editUser($data);
                if (!$model) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => 'Information updated.'));
                    return $app->redirect($app['url_generator']->generate("/posts/"), 301);
                }
            }

            return $app['twig']->render('users/edit.twig', array('form' => $form->createView()));
        } else {
            return $app->redirect($app['url_generator']->generate('/users/add'), 301);
        }
    }

    /*
     *
     */

    public function delete(Application $app, Request $request)
    {
        $id = (int)$request->get('id', 0);

        $user = $this->_model->getUser($id);

        $data = array();

        if (count($user)) {
            $redirect = $app->redirect($app['url_generator']->generate('/users/'), 301);

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('iduser', 'hidden', array(
                    'data' => $id,
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $model = $this->_model->deleteUser($data);
                if (!$model) {
                    $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => ' User delete'));
                    return $app->redirect($app['url_generator']->generate("/auth/login"), 301);
                }
            }

            return $app['twig']->render('users/delete.twig', array('form' => $form->createView(), 'redirect' => $redirect));

        }
        $app->notFound();

    }

    public function view(Application $app, Request $request)
    {

        $id = (int)$request->get('id', 0);

        $user = $this->_model->getUser($id);
        if (count($user)) {
            return $app['twig']->render('users/view.twig', array('user' => $user));
        } else {
            $app->notFound();
        }
    }

    public function password(Application $app, Request $request)
    {

        $id = (int)$request->get('id', 0);

        $user = $this->_model->getUserById($id);

        if (count($user)) {

            $data = array();

            $form = $app['form.factory']->createBuilder('form', $data)
                ->add('password', 'password', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('new_password', 'password', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->add('confirm_new_password', 'password', array(
                    'constraints' => array(new Assert\NotBlank(), new Assert\Length(array('min' => 5)))
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isValid()){
                $data = $form->getData();

                if ($data['new_password'] === $data['confirm_new_password']) {

                    $data['new_password'] = $app['security.encoder.digest']->encodePassword("{$data['new_password']}", '');

                    $model = $this->_model->changePassword($data, $id);

                    if (!$model) {
                        $app['session']->getFlashBag()->add('message', array('type' => 'success', 'content' => ' password had been changed'));
                      //  return $app->redirect($app['url_generator']->generate("/auth/login"), 301);
                    }

                } else {
                    $app['session']->getFlashBag()->add('message', array('type' => 'warning', 'content' => 'Passwords are not the same'));
                    return $app['twig']->render('users/edit.twig', array('form' => $form->createView()));
                }
            }
        } else {
         //   return $app->redirect($app['url_generator']->generate('/auth/login'), 301);
        }
        return $app['twig']->render('users/edit.twig', array('form' => $form->createView()));
    }

    /**
     * This method checkes if user is logged in.
     *
     * @param application $app
     * @return bool
     */
    protected function isLoggedIn(Application $app)
    {
        if (null === $user = $app['session']->get('user')) {
            return false;
        } else {
            return true;
        }
    }


}